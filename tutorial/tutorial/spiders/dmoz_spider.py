import scrapy
from tutorial.items import DmozItem
from tutorial.items import JdGoodItem

class DmozSpider(scrapy.spider.Spider):
    name = "dmoz"
    allowed_domains = ["dmoz.org"]
    #start_urls = ["http://www.dmoz.org/Computers/Programming/Languages/Python/Books/"]
    start_urls = ['http://item.jd.com/996967.html']
    def parse(self, response):
        # filename = response.url.split("/")[-2]+".html"
        # with open(filename, "wb") as f:
        #     f.write(response.body)

        # for sel in response.xpath("//ul/li"):
        #     title = sel.xpath("a/text()").extract()
        #     link = sel.xpath("a/@href").extract()
        #     desc = sel.xpath("text()").extract()
        #
        #     print title, link
        #     print

        # for sel in response.xpath("//ul/li"):
        #     item = DmozItem()
        #     item["title"] = sel.xpath("a/text()").extract()
        #     item["link"] = sel.xpath('a/@href').extract()
        #     item["desc"] = sel.xpath('text()').extract()
        #     yield item

        item = JdGoodItem()
        item["name"] = response.css("[id=name]>h1::text").extract()
        item["price"] = response.css("[id=jd-price]::text").extract()

        return item
