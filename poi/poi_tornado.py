# -*- coding:utf-8 -*-
import tornado
import tornado.httpclient
import urllib2
import time

import os
import codecs
import json


dir_path = os.path.dirname(os.path.abspath(__file__))
f_res = open(dir_path + '/f_res.txt', 'w')
f_res.write("what the fuck \n")


def get_lines(path):
    f = codecs.open(path, 'r', 'gbk')
    while 1:
        line = f.readline()
        if line:
            yield line
        else:
            break
        
def get_urls(path):
    keys = [
        "NUOBZ-URL3I-TEKGA-5EJMG-J5WQS-Z5BAZ",
        "IHOBZ-SFJA3-SXJ3R-37TEC-UWMY6-SBBVT",
        "3ELBZ-YBUHU-T5SVH-2NQUP-MQWC5-IVBDQ",
        "JJJBZ-ZPORJ-4VKF5-FV3WG-JUTNZ-H6FIJ",
        "GOBBZ-L2XHU-U5JVO-2VTJ2-4CSC5-NSFAY",
        "3EFBZ-5TFH4-ZJDU2-XIC54-ZZ27O-FHBYT",
        "AGABZ-GARRD-MBJ4Y-PN5QJ-BYBLT-4YBMI",
        "HQSBZ-LHXRJ-HVVFS-FC6GN-KWSN3-AUB3X",
        "NTTBZ-VR53J-4TPFZ-KEGQD-C5PAJ-6SF6K",
        "ZSTBZ-O3MHV-GPXPO-UE6AU-XSNB7-AQFWZ"]
        
    lines = get_lines(path)
    
    index = -1
    for line in lines:
        if line:
            index += 1
            line = line.split()
            lng_line = line[-2]  #经度
            lat_line = line[-1]  #纬度
            
            lng = lng_line[:-6] + '.' + lng_line[-6:]
            lat = lat_line[:-6] + '.' + lat_line[-6:]
            url = 'http://apis.map.qq.com/ws/geocoder/v1/?location=%s,%s&key=%s&get_poi=1&output=json' % (lng, lat, keys[index % 10])
            yield url

def handle_request(response):
    f_res.write("[status] : \n")
    print '[ response ]'
    if response.error:
        print "[ error ]"
    else:
        data = json.loads(response.body)
        f_res.write("[status] : "+str(data["status"])+"\n")
        f_res.flush()
        # print "data: %s" % response.body
        print "[ status] : %s" % data["status"]
        
if __name__ == '__main__':
    urls = get_urls(dir_path + "/shenzhenLBSInfo")
    http_client = tornado.httpclient.AsyncHTTPClient()

    count = 0
    for url in urls:
        count += 1
        if not count % 10:
            time.sleep(2)
        print '[ count ]: %s' % count
        print '[ fetching ]: %s' % url
        http_client.fetch(url, handle_request)
    
    tornado.ioloop.IOLoop.instance().start()
        
     

