# -*- coding:utf-8 -*-
import Queue
import threading
import urllib2
import time
import httplib

import os
import codecs
import json

gindex = 0
oindex = 0
times_count = 0

def get_lines(path):
    f = codecs.open(path, 'r', 'gbk')
    while 1:
        line = f.readline()
        if line:
            yield line
        else:
            break
        
def get_items(keys, path):
    keys_len = len(keys)  
    lines = get_lines(path)
    
    index = 0
    for line in lines:
        if line:
            index += 1
            line = line.split()
            lng_line = line[-2]  #经度
            lat_line = line[-1]  #纬度
            
            lng = lng_line[:-6] + '.' + lng_line[-6:]
            lat = lat_line[:-6] + '.' + lat_line[-6:]
            url = 'http://apis.map.qq.com/ws/geocoder/v1/?location=%s,%s&key=%s&get_poi=1&output=json' % (lng, lat, keys[index % keys_len])
            #print "[ key ]: " , keys[index % 10] 
            yield (url, line)
 
def poiFetch(conn, url, lbs, f_err):
    page = None
    try:
        conn.request('GET', url)
        r = conn.getresponse()
        if r.status == 200:
            page = r.read()
            data = json.loads(page)
            
            #这里统计因为请求过快导致的status值不等于0的情况
            if data["status"] != 0:
                print '[StatusError In Fetch]: ', lbs
                f_err.write('[StatusError In Fetch]: ' "lbs")
                return None
                
            poi =  data['result']['pois'][0]  
            return (poi['id'], poi['title'], poi['address'], poi['category'], str(poi['location']['lat']), str(poi['location']['lng']))
        else:
            print '[!200Error In Fetch]: ', lbs
            f_err.write('[!200Error In Fetch]: ' "lbs")
            return None
    except httplib.HTTPException, e:
        print '[HTTPException In Fetch]: ', lbs
        f_err.write('[HTTPException In Fetch]: ' "lbs")
        return None
    except KeyError:
        print '[KeyError IN Fetch]: ', lbs
        f_err.write('[KeyError In Fetch]: ' "lbs")
        return None
    except IndexError:
        print '[IndexError IN Fetch]: ', lbs
        f_err.write('[IndexError In Fetch]: ' "lbs")
        return None
    except Exception:
        print '[Exception IN Fetch]: ', lbs
        f_err.write('[Exception In Fetch]: ' "lbs")
        return None
    finally:
        conn.close()

class PoiThread(threading.Thread):
    def __init__(self, item_queue, p_host, p_port, f_suc, f_err, f_url):
        threading.Thread.__init__(self)
        self.item_queue = item_queue
        self.p_host = p_host
        self.p_port = p_port
        self.f_err = f_err
        self.f_suc = f_suc
        self.f_url = f_url
    
    def run(self):
        conn = httplib.HTTPConnection(self.p_host, self.p_port)
        print '[Enter Run ]'
        while True:
            try:
                item = self.item_queue.get()
                url = item[0]
                lbs = item[1]
                res = poiFetch(conn, url, lbs, self.f_err)
                
                if res is None:
                    continue
                line = "success lbs"
                
                self.f_suc.write(line)
                self.item_queue.task_done()
            except Queue.Empty:
                break

class ProduceThread(threading.Thread):
    def __init__(self, item_queue, items):
        threading.Thread.__init__(self)
        self.item_queue = item_queue
        self.items = items
        
    def run(self):
        i = 0
        for item in self.items:
            i+=1    
            if(i > 500):
                break
            #print "%s url: %s" % (i, item[0])
            self.item_queue.put(item)
 
if __name__ == '__main__':
    keys = [
        "NUOBZ-URL3I-TEKGA-5EJMG-J5WQS-Z5BAZ","IHOBZ-SFJA3-SXJ3R-37TEC-UWMY6-SBBVT",
        "3ELBZ-YBUHU-T5SVH-2NQUP-MQWC5-IVBDQ","JJJBZ-ZPORJ-4VKF5-FV3WG-JUTNZ-H6FIJ",
        "GOBBZ-L2XHU-U5JVO-2VTJ2-4CSC5-NSFAY","3EFBZ-5TFH4-ZJDU2-XIC54-ZZ27O-FHBYT",
        "AGABZ-GARRD-MBJ4Y-PN5QJ-BYBLT-4YBMI","HQSBZ-LHXRJ-HVVFS-FC6GN-KWSN3-AUB3X",
        "NTTBZ-VR53J-4TPFZ-KEGQD-C5PAJ-6SF6K","ZSTBZ-O3MHV-GPXPO-UE6AU-XSNB7-AQFWZ"] 
    dir_path = os.path.dirname(os.path.abspath(__file__))
    f_res = open(dir_path + '/f_res.txt', 'w')
    f_suc = open(dir_path + '/poi_suc.txt', 'w') #记录成功的数据
    f_err = open(dir_path + '/poi_err.txt', 'w') #记录错误的lbs以及出错的地方和原因
    f_url = open(dir_path + '/poi_url.txt', 'w') #记录lbs和url的对应关系
    
    lbs_path = dir_path + '/shenzhenLBSInfo' #lbs文件路径   
    proxys = ( ('172.27.28.234', 8080), ('10.129.128.150', 8080), ('10.133.0.241', 8080), ('10.198.11.118', 8080) ) #4个代理
    
    #items是个生成器
    items = get_items(keys, lbs_path)
    item_queue = Queue.Queue()
    #f_suc_queue = Queue.Queue()
    #f_err_queue = Queue.Queue()
    
    #1生产者线程
    pt = ProduceThread(item_queue, items)
    pt.start()
    
    #4个消费者线程
    for p in proxys:
        t = PoiThread(item_queue, p[0], p[1], f_suc, f_err, f_url)
        t.setDaemon(True)
        t.start()
    item_queue.join()
        
    