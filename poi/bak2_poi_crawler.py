# -*- coding:utf-8 -*-
import json
import httplib
import codecs
import os
import sys
import time

reload(sys)
try:
    sys.setdefaultencoding('utf-8')
except Exception, e:
    print e


def lbs_info_gen(path):
    f = codecs.open(path, 'r', 'gbk')
    while 1:
        line = f.readline()
        if line:
            yield line
        else:
            break


def poiFetch(conn, url, lbs, f_err):
    page = None
    try:
        conn.request('GET', url)
        r = conn.getresponse()
        if r.status == 200:
            page = r.read()
            poi = json.loads(page)
            
            poi_id = poi['result']['pois'][0]['id']
            title = poi['result']['pois'][0]['title']
            address = poi['result']['pois'][0]['address']
            category = poi['result']['pois'][0]['category']
            lat = poi['result']['pois'][0]['location']['lat']
            lng = poi['result']['pois'][0]['location']['lng']
            return (poi_id, title, address, category, str(lat), str(lng))
        else:
            #conn.close()
            print '[!200]: ', lbs
            return None
    except httplib.HTTPException, e:
        print '[HTTPException IN Fetch]: ', lbs
        return None
    except KeyError:
        print '[KeyError IN Fetch]: ', lbs
        return None
    except IndexError:
        print '[IndexError IN Fetch]: ', lbs
        return None
    except Exception:
        print '[Exception IN Fetch]: ', lbs
        return None


if __name__ == "__main__":
    keys = [
        "NUOBZ-URL3I-TEKGA-5EJMG-J5WQS-Z5BAZ",
        "IHOBZ-SFJA3-SXJ3R-37TEC-UWMY6-SBBVT",
        "3ELBZ-YBUHU-T5SVH-2NQUP-MQWC5-IVBDQ",
        "JJJBZ-ZPORJ-4VKF5-FV3WG-JUTNZ-H6FIJ",
        "GOBBZ-L2XHU-U5JVO-2VTJ2-4CSC5-NSFAY",
        "3EFBZ-5TFH4-ZJDU2-XIC54-ZZ27O-FHBYT",
        "AGABZ-GARRD-MBJ4Y-PN5QJ-BYBLT-4YBMI",
        "HQSBZ-LHXRJ-HVVFS-FC6GN-KWSN3-AUB3X",
        "NTTBZ-VR53J-4TPFZ-KEGQD-C5PAJ-6SF6K",
        "ZSTBZ-O3MHV-GPXPO-UE6AU-XSNB7-AQFWZ"]
        
    dir_path = os.path.dirname(os.path.abspath(__file__))
    f_suc = open(dir_path + '/poi_suc.txt', 'w')
    f_err = open(dir_path + '/poi_err.txt', 'w')
    #lbs文件line生成器
    f_lbs = lbs_info_gen(dir_path + '/shenzhenLBSInfo')
    
    conn = httplib.HTTPConnection('apis.map.qq.com', timeout=10)
    
    index = 0
    for lbs in f_lbs:
        index += 1
        fs = lbs.split()
        try:
            #这里可能会出现IndexError
            lng = fs[-2]  #经度
            lat = fs[-1]  #纬度
            
            #如果longitude和latitude不是数字，则无意义，抛出ValueError异常
            if not lng.isdigit() or not lat.isdigit():
                raise ValueError("经纬度格式非法")
            else:
                longitude = lng[:-6] + '.' + lng[-6:]
                latitude = lat[:-6] + '.' + lat[-6:]
        except IndexError, e:
            print '[IndexError]:', lbs 
            f_err.write(lbs)
            continue
        except ValueError, e:
            print '[ValueError]:', lbs
            f_err.write(lbs)
            continue
        except Exception, e:
            print '[Exception]:', lbs
            f_err.write(lbs)
            continue
        else:
            url = '/ws/geocoder/v1/?location=%s,%s&key=%s&get_poi=1&output=json' % (longitude, latitude, keys[index % 10])
            print "[%d]: http://apis.map.qq.com%s" % (index, url)
            res = poiFetch(conn, url, lbs, f_err)
            conn.close()
            if res is None:
                f_err.write(lbs)
                continue
            line = lbs[:-1] + '\t' + '\t'.join(res) + '\n'
            f_suc.write(line)
            #time.sleep(0)
    
    f_suc.close()
    f_err.close()

        