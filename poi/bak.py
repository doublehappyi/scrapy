#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json
import urllib2
import httplib
import codecs
import sys
import time
reload(sys)                         
sys.setdefaultencoding('utf-8')     
# page = urllib2.urlopen(url="http://apis.map.qq.com/ws/geocoder/v1/?location=22.545142,114.088988&key=ZFKBZ-TOXAW-SYWRM-OMUZY-WO5ST-W3FXG&get_poi=1&output=json", timeout=300).read()
#  
# print page
# poi = json.loads(page)
# print poi['result']['pois'][0]['id']
# print poi['result']['pois'][0]['title']
# print poi['result']['pois'][0]['address']

def loadLBSInfo(path):
    lbs_list = []
    for line in codecs.open(path, 'r', 'gbk'):
#         if line.find('深圳') != -1:
        lbs_list.append(line)
    return lbs_list

#"ZFKBZ-TOXAW-SYWRM-OMUZY-WO5ST-W3FXG",
keys = [
        "NUOBZ-URL3I-TEKGA-5EJMG-J5WQS-Z5BAZ",
        "IHOBZ-SFJA3-SXJ3R-37TEC-UWMY6-SBBVT",
        "3ELBZ-YBUHU-T5SVH-2NQUP-MQWC5-IVBDQ",
        "JJJBZ-ZPORJ-4VKF5-FV3WG-JUTNZ-H6FIJ",
        "GOBBZ-L2XHU-U5JVO-2VTJ2-4CSC5-NSFAY",
        "3EFBZ-5TFH4-ZJDU2-XIC54-ZZ27O-FHBYT",
        "AGABZ-GARRD-MBJ4Y-PN5QJ-BYBLT-4YBMI",
        "HQSBZ-LHXRJ-HVVFS-FC6GN-KWSN3-AUB3X",
        "NTTBZ-VR53J-4TPFZ-KEGQD-C5PAJ-6SF6K",
        "ZSTBZ-O3MHV-GPXPO-UE6AU-XSNB7-AQFWZ"]

conn = httplib.HTTPConnection('apis.map.qq.com')
def poiFetch(conn, longitude, latitude, index):
    longitude = longitude[:-6] + '.' + longitude[-6:]
    latitude  =  latitude[:-6]  + '.' + latitude[-6:]
    url = '/ws/geocoder/v1/?location=%s,%s&key=%s&get_poi=1&output=json' % (longitude,latitude, keys[index % 10])
    print "[%d] http://apis.map.qq.com%s" % (index, url)
    
    page = None
    try:
        conn.request('GET', url)
        r = conn.getresponse()
        if r.status == 200:
            page = r.read()
            poi = json.loads(page)
            print 'ok:', longitude, latitude
            id      = poi['result']['pois'][0]['id']
            title   = poi['result']['pois'][0]['title']
            address = poi['result']['pois'][0]['address']
            category= poi['result']['pois'][0]['category']
            lat     = poi['result']['pois'][0]['location']['lat']
            lng     = poi['result']['pois'][0]['location']['lng']
            return (id, title, address , category, str(lat), str(lng))
        else:
            self.conn.close()
            print '!200:', longitude, latitude
            return None
    except KeyError,e:
        print >> sys.stderr, page
        return None
    except:
        import traceback
        traceback.print_exc(file=sys.stderr)     
        conn.close()       
        conn = httplib.HTTPConnection('apis.map.qq.com')
        print 'err:', longitude, latitude
        return None

if __name__ == "__main__":
    lbs_list = loadLBSInfo("shenzhenLBSInfo")
     
    f = open("all_poi.txt", 'w')
    f_error = open('error.txt', 'w')
    for index, lbs in enumerate(lbs_list):
        fs = lbs.split('\t')
        longitude = fs[4] #经度
        latitude  = fs[5][:-1]  #纬度
        print "[%s]longitude: %s, latitude: %s" % (index, longitude, latitude)
        #time.sleep(0.01)
        #continue
        res = poiFetch(conn, longitude, latitude, index)
        time.sleep(0.08)
        if res is None:
            f_error.write(lbs)
            continue
        line = lbs[:-1] + '\t' + '\t'.join(res) + '\n'
        f.write(line)
    f.close()
    f_error.close()

        