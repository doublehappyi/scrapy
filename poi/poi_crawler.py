# -*- coding:utf-8 -*-
import json
import httplib
import codecs
import os
import sys
import time
import urllib2

reload(sys)
try:
    sys.setdefaultencoding('utf-8')
except Exception, e:
    print e


def lbs_info_gen(path):
    f = codecs.open(path, 'r', 'gbk')
    while 1:
        line = f.readline()
        if line:
            yield line
        else:
            break


def poiFetch(conn, url, lbs, f_err):
    page = None
    try:
        conn.request('GET', url)
        r = conn.getresponse()
        if r.status == 200:
            page = r.read()
            data = json.loads(page)
            
            #这里统计因为请求过快导致的status值不等于0的情况
            if data["status"] != 0:
                print '[StatusError In Fetch]: ', lbs
                f_err.write('[StatusError In Fetch]: ' + lbs)
                return None
                
            poi =  data['result']['pois'][0]  
            return (poi['id'], poi['title'], poi['address'], poi['category'], str(poi['location']['lat']), str(poi['location']['lng']))
        else:
            print '[!200Error In Fetch]: ', lbs
            f_err.write('[!200Error In Fetch]: ' + lbs)
            return None
    except httplib.HTTPException, e:
        print '[HTTPException In Fetch]: ', lbs
        f_err.write('[HTTPException In Fetch]: ' + lbs)
        return None
    except KeyError:
        print '[KeyError IN Fetch]: ', lbs
        f_err.write('[KeyError In Fetch]: ' + lbs)
        return None
    except IndexError:
        print '[IndexError IN Fetch]: ', lbs
        f_err.write('[IndexError In Fetch]: ' + lbs)
        return None
    except Exception:
        print '[Exception IN Fetch]: ', lbs
        f_err.write('[Exception In Fetch]: ' + lbs)
        return None
    finally:
        conn.close()

if __name__ == "__main__":
    keys = [
        "NUOBZ-URL3I-TEKGA-5EJMG-J5WQS-Z5BAZ",
        "IHOBZ-SFJA3-SXJ3R-37TEC-UWMY6-SBBVT",
        "3ELBZ-YBUHU-T5SVH-2NQUP-MQWC5-IVBDQ",
        "JJJBZ-ZPORJ-4VKF5-FV3WG-JUTNZ-H6FIJ",
        "GOBBZ-L2XHU-U5JVO-2VTJ2-4CSC5-NSFAY",
        "3EFBZ-5TFH4-ZJDU2-XIC54-ZZ27O-FHBYT",
        "AGABZ-GARRD-MBJ4Y-PN5QJ-BYBLT-4YBMI",
        "HQSBZ-LHXRJ-HVVFS-FC6GN-KWSN3-AUB3X",
        "NTTBZ-VR53J-4TPFZ-KEGQD-C5PAJ-6SF6K",
        "ZSTBZ-O3MHV-GPXPO-UE6AU-XSNB7-AQFWZ"]
        
    dir_path = os.path.dirname(os.path.abspath(__file__))
    f_suc = open(dir_path + '/poi_suc.txt', 'w') #记录成功的数据
    f_err = open(dir_path + '/poi_err.txt', 'w') #记录错误的lbs以及出错的地方和原因
    f_url = open(dir_path + '/poi_url.txt', 'w') #记录lbs和url的对应关系
    #lbs文件line生成器
    f_lbs = lbs_info_gen(dir_path + '/shenzhenLBSInfo')
    
    #conn = httplib.HTTPConnection('apis.map.qq.com', timeout=10)
    proxy_host = "10.133.0.241"
    proxy_port = 8080
    conn = httplib.HTTPConnection(proxy_host, proxy_port)
    
    index = 0  #记录当前已经是第几条数据了
    count = 0  #和t_start一起，每10次请求计时一次，看是否超出10次/s的限制
    t_start = 0
    
    st = time.time() #记录请求数据的开始时间
    for lbs in f_lbs:
        index += 1
        fs = lbs.split()
        try:
            #这里可能会出现IndexError
            lng = fs[-2]  #经度
            lat = fs[-1]  #纬度
            
            #如果longitude和latitude不是数字，则无意义，抛出ValueError异常
            if not lng.isdigit() or not lat.isdigit():
                raise ValueError("经纬度格式非法")
            else:
                longitude = lng[:-6] + '.' + lng[-6:]
                latitude = lat[:-6] + '.' + lat[-6:]
        except IndexError, e:
            print '[IndexError In LBS]:', lbs 
            f_err.write('[IndexError In LBS]:' + lbs )#你好
            continue
        except ValueError, e:
            print '[ValueError In LBS]:', lbs
            f_err.write('[ValueError In LBS]:' + lbs)
            continue
        except Exception, e:
            print '[Exception In LBS]:', lbs
            f_err.write('[Exception In LBS]:' + lbs)
            continue
        else:
            url = 'http://apis.map.qq.com/ws/geocoder/v1/?location=%s,%s&key=%s&get_poi=1&output=json' % (longitude, latitude, keys[index % 10])
            f_url.write('[lbs]:'+lbs)
            f_url.write('[url[:'+url+'\n')
            print "[%d]: http://apis.map.qq.com%s" % (index, url)
            
            #记录请求次数开始
            if not t_start and not count:
                t_start = time.time()
                count = 0
            count+=1 #请求发出就开始记录次数
            
            res = poiFetch(conn, url, lbs, f_err)
            
            if res is None:
                #f_err.write(lbs)
                continue
            
            line = lbs[:-1] + '\t' + '\t'.join(res) + '\n'
            f_suc.write(line)
            
            #计次数结束：10次请求用时是否超过了1s，如果超过，time.sleep补够1s，否则，复位count和t_start之后再继续下一次计请求次数
            if count == 10:
                interval = time.time() - t_start
                if(interval < 1):
                    time.sleep(1 - interval)
                t_start = 0
                count = 0
    
    f_suc.close()
    f_err.close()
    f_url.close()
    print "total time: %s" % (time.time() - st)

        